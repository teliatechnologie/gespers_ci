<?php
namespace test\models;
use Doctrine\ORM\Mapping as ORM,
    Gedmo\Mapping\Annotation as Gedmo;
    
/**
 * @ORM\Table(name="regions")
 * @ORM\Entity(repositoryClass="models\RegionsRepository")
 */
class Regions {
	
    /**
     * @ORM\Id
     * @ORM\Column(name="idregion", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $codeRegion;
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $libelleRegion;
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $chefLieuRegion; 
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $dateCrea;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $dateUpdate;     

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codeRegion
     *
     * @param string $codeRegion
     * @return Regions
     */
    public function setCodeRegion($codeRegion)
    {
        $this->codeRegion = $codeRegion;
        return $this;
    }

    /**
     * Get codeRegion
     *
     * @return string 
     */
    public function getCodeRegion()
    {
        return $this->codeRegion;
    }

    /**
     * Set libelleRegion
     *
     * @param string $libelleRegion
     * @return Regions
     */
    public function setLibelleRegion($libelleRegion)
    {
        $this->libelleRegion = $libelleRegion;
        return $this;
    }

    /**
     * Get libelleRegion
     *
     * @return string 
     */
    public function getLibelleRegion()
    {
        return $this->libelleRegion;
    }

    /**
     * Set chefLieuRegion
     *
     * @param string $chefLieuRegion
     * @return Regions
     */
    public function setChefLieuRegion($chefLieuRegion)
    {
        $this->chefLieuRegion = $chefLieuRegion;
        return $this;
    }

    /**
     * Get chefLieuRegion
     *
     * @return string 
     */
    public function getChefLieuRegion()
    {
        return $this->chefLieuRegion;
    }

    /**
     * Set dateCrea
     *
     * @param datetime $dateCrea
     * @return Regions
     */
    public function setDateCrea($dateCrea)
    {
        $this->dateCrea = $dateCrea;
        return $this;
    }

    /**
     * Get dateCrea
     *
     * @return datetime 
     */
    public function getDateCrea()
    {
        return $this->dateCrea;
    }

    /**
     * Set dateUpdate
     *
     * @param datetime $dateUpdate
     * @return Regions
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;
        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return datetime 
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }
}