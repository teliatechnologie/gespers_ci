<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use Doctrine\Common\ClassLoader,
    Doctrine\ORM\Configuration,
    Doctrine\ORM\EntityManager,
    Doctrine\ORM\Mapping\Driver\AnnotationDriver,
    Doctrine\Common\Cache\ArrayCache,
    Doctrine\Common\EventManager,
    Doctrine\Common\Annotations\AnnotationReader,
    Doctrine\ORM\Tools\SchemaTool,
    Doctrine\Common\Annotations\AnnotationRegistry,
    Doctrine\Common\Annotations\CachedReader,
    Doctrine\DBAL\Event\Listeners\MysqlSessionInit,
    Doctrine\ORM\Tools\Setup,
    Doctrine\DBAL\Logging\EchoSQLLogger,
    Gedmo\Timestampable\TimestampableListener,
    Gedmo\Sluggable\SluggableListener,
    Gedmo\Tree\TreeListener;

function load_hmvc_models_namespace ($dir) {
    if ($handle = opendir($dir)) {
        while (false !== ($file = readdir($handle))) {
            if ($file != "." && $file != ".." && is_dir($dir.$file)) {
                if (is_dir(APPPATH.'modules/'. $file .'/models'))
                {
                     $entitiesClassLoader = new ClassLoader($file, APPPATH.'modules/'.$file);
                     $entitiesClassLoader->register();
                }
            }
        }
    }
}
  function load_hmvc_models (&$models,$dir) {
    if ($handle = opendir($dir)) {
        while (false !== ($file = readdir($handle))) {
            if ($file != "." && $file != ".." && is_dir($dir.$file)) {
                if (is_dir(APPPATH.'modules/'. $file .'/models'))
                {
                     array_push($models, APPPATH.'modules/'. $file .'/models');
                      
                }
            }
        }
    }
}

class Doctrine {

  public $em = null;
  

  public function __construct()
  {
    // load database configuration from CodeIgniter
    require APPPATH.'config/database.php';

    // Set up class loading. You could use different autoloaders, provided by your favorite framework,
    // if you want to.
    require_once APPPATH .'third_party/Doctrine/Common/ClassLoader.php';

    $doctrineClassLoader = new ClassLoader('Doctrine',   APPPATH .'third_party');
    $doctrineClassLoader->register();
    $symfonyClassLoader = new ClassLoader('Symfony', APPPATH .'third_party/Doctrine');
    $symfonyClassLoader->register();
    $gedmoClassLoader= new ClassLoader('Gedmo', APPPATH.'third_party');
    $gedmoClassLoader->register();
    $entitiesClassLoader = new ClassLoader('models', rtrim(APPPATH, "/" ));
    $entitiesClassLoader->register();
    $proxiesClassLoader = new ClassLoader('Proxies', APPPATH.'models/proxies');
    $proxiesClassLoader->register();
    
    //load_hmvc_models_namespace(APPPATH.'modules/');
    foreach (glob(APPPATH.'modules/*', GLOB_ONLYDIR) as $m) 
    {
        $module = str_replace(APPPATH.'modules/', '', $m);
        $entitiesClassLoader = new ClassLoader($module, APPPATH.'modules');
        $entitiesClassLoader->register();
        
    }
    
   /* if ($handle = opendir(APPPATH.'modules/')) 
    {
        while (false !== ($entry = readdir($handle))) 
        {
            if ($entry != "." && $entry != "..") 
            {
                $entitiesClassLoader = new ClassLoader($entry, APPPATH.'modules');
                $entitiesClassLoader->register();
            }
        }
        closedir($handle);
    }*/
    
    
    $isDevMode=true;
    // Set up caches
    //$config = Setup::createConfiguration($isDevMode);
    $config= new Configuration();
    $cache = new ArrayCache;
    $annotationReader = new AnnotationReader;
    $cachedAnnotationReader = new CachedReader($annotationReader, $cache);
    
    $config->setMetadataCacheImpl($cache);
    $models = array(APPPATH.'models');
   /* if ($handle = opendir(APPPATH.'modules/')) 
    {
        while (false !== ($entry = readdir($handle))) 
        {
            if ($entry != "." && $entry != "..") 
            {
                $m=APPPATH.'modules/'.$entry.'/models';
                 array_push($models, $entry);
                  //echo $m.'<br/>';
            }
        }
        closedir($handle);
    }*/
    
 //load_hmvc_models($models, APPPATH.'modules/');
   foreach (glob(APPPATH.'modules/*/models', GLOB_ONLYDIR) as $m){ 
       array_push($models, $m);
     
    }
    
    /*$driver = new YamlDriver(array(APPPATH.'yml'));
    $config->setMetadataDriverImpl($driver);*/
    
    $driverImpl = new AnnotationDriver(new AnnotationReader(), $models);
    AnnotationRegistry::registerLoader('class_exists');
    $config->setMetadataDriverImpl($driverImpl);
    
    
    
    $config->setQueryCacheImpl($cache);

    //$config->setQueryCacheImpl($cache);

    $evm = new EventManager();
    
    // sluggable
    $sluggableListener = new SluggableListener();
    $sluggableListener->setAnnotationReader($cachedAnnotationReader);
    $evm->addEventSubscriber($sluggableListener);
    
     // tree
    $treeListener = new TreeListener;
    $treeListener->setAnnotationReader($cachedAnnotationReader);
    $evm->addEventSubscriber($treeListener);
    
     // timestampable
    $timestampableListener = new TimestampableListener;
    $timestampableListener->setAnnotationReader($cachedAnnotationReader);
    $evm->addEventSubscriber($timestampableListener);
    
    // Proxy configuration
    $config->setProxyDir(APPPATH.'/models/proxies');
    $config->setProxyNamespace('Proxies');

    // Set up logger
    /*$logger = new EchoSQLLogger;
    $config->setSQLLogger($logger);*/
    
    /*$logger = new \Doctrine\DBAL\Logging\Profiler;
    $config->setSQLLogger($logger);*/
    
    $config->setAutoGenerateProxyClasses( TRUE );

    // Database connection information
    $connectionOptions = array(
               'driver' => 'pdo_mysql',
               'user' =>     $db[$active_group]['username'],
               'password' => $db[$active_group]['password'],
               'host' =>     $db[$active_group]['hostname'],
               'dbname' =>   $db[$active_group]['database'],
               'charset'=>  $db[$active_group]['char_set'],
               'driverOptions' => array(
                    'charset'=>  $db[$active_group]['char_set'],
               ),
           );
    
   
    // Create EntityManager
    $this->em = EntityManager::create($connectionOptions, $config,$evm);
    $this->em->getEventManager()->addEventSubscriber(new MysqlSessionInit('utf8', 'utf8_unicode_ci'));
    
    
    // Schema Tool
    $this->tool = new SchemaTool($this->em);
    
    
  }
}