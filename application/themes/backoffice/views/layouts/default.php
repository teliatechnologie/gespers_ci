<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php if(isset($template['title'])){ echo $template['title'];}?></title>
    </head>
    <body>
        <div class="container">
            <div class="col-md-3">
                <?php if(isset($template['partials']['menu'])){ echo $template['partials']['menu'];}?>
            </div>
            <div class="col-md-9">
                <?php
                // put your code here
                if(isset($template['body']))
                {
                    echo $template['body'];
                }
                ?>
            </div>
        </div>
    </body>
</html>
