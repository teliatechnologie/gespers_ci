<?php
class Test extends  Admin_Controller
{
	function __construct()
        {
          parent::__construct();
          $this->data['current_user_Emploi'] =$this->ion_auth->user()->row()->Emploi;
          $this->data['current_user_noms'] =$this->ion_auth->user()->row()->Nom;
          $this->load->model('Profil_model');

        }

	function index()
	{
		
		$this->load->view('test/ajax_test_view');
	}

	public function get_something()
	{
		$this->load->library('ajax');

		$arr['something'] = 'Something Good';

		if ($this->input->post('something_id') == '2')
		{
			$arr['something'] = 'Something Better';
		}

		$this->ajax->output_ajax($arr);
	}
}